## Prestashop configuration

# Prestashop password test: cmh<C}9r:Gj>._$tU,Ny#T

# Redirect 404 errors
error_page 404 /index.php?controller=404;

# Images.
rewrite ^/(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$1$2.jpg last;
rewrite ^/(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$1$2$3.jpg last;
rewrite ^/(\d)(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$3/$1$2$3$4.jpg last;
rewrite ^/(\d)(\d)(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$3/$4/$1$2$3$4$5.jpg last;
rewrite ^/(\d)(\d)(\d)(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$3/$4/$5/$1$2$3$4$5$6.jpg last;
rewrite ^/(\d)(\d)(\d)(\d)(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$3/$4/$5/$6/$1$2$3$4$5$6$7.jpg last;
rewrite ^/(\d)(\d)(\d)(\d)(\d)(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$3/$4/$5/$6/$7/$1$2$3$4$5$6$7$8.jpg last;
rewrite ^/(\d)(\d)(\d)(\d)(\d)(\d)(\d)(\d)(-[\w-]+)?/.+\.jpg$ /img/p/$1/$2/$3/$4/$5/$6/$7/$8/$1$2$3$4$5$6$7$8$9.jpg last;
rewrite ^/c/([\w.-]+)/.+\.jpg$ /img/c/$1.jpg last;

# AlphaImageLoader for IE and FancyBox.
rewrite ^images_ie/?([^/]+)\.(gif|jpe?g|png)$ js/jquery/plugins/fancybox/images/$1.$2 last;

# Web service API.
rewrite ^/api/?(.*)$ /webservice/dispatcher.php?url=$1 last;

# Installation sandbox.
rewrite ^(/install(?:-dev)?/sandbox)/.* /$1/test.php last;

location / {
    try_files $uri $uri/ /index.php$is_args$args;
}

# Replace 'admin-dev' in this block with the name of your admin directory.
location /admin-dev/ {
    try_files $uri $uri/ /admin-dev/index.php$is_args$args;
}

# .htaccess, .DS_Store, .htpasswd, etc.
location ~ /\. {
    deny all;
}

# Source code directories.
location ~ ^/(app|bin|cache|classes|config|controllers|docs|localization|override|src|tests|tools|translations|var|vendor)/ {
    deny all;
}

# vendor in modules directory.
location ~ ^/modules/.*/vendor/ {
    deny all;
}

# Prevent exposing other sensitive files.
location ~ \.(log|tpl|twig|sass|yml)$ {
    deny all;
}

# Prevent injection of PHP files.
location /img {
    location ~ \.php$ { deny all; }
}
location /upload {
    location ~ \.php$ { deny all; }
}
