.PHONY: up
up: # Launch the container en mode tty
	docker compose up --build

.PHONY: exec
exec:
	docker compose exec -it --privileged ansible bash

.PHONY: build
build:
	docker build -t ansible-test .

.PHONY: run
run:
	docker run -it --privileged ansible-test bash