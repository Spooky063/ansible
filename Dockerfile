FROM ubuntu:22.04 AS ansible-local

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    vim \
    python3 \
    python3-pip \
    python3-apt \
    sudo \
    curl \
    git \
    ssh \
    software-properties-common \
    && apt-get clean

RUN pip3 install ansible

RUN apt-get update && apt-get install -y docker.io

RUN groupadd -g 1000 ansible

RUN useradd -rms /bin/bash -u 1000 -g 1000 ansible && echo "ansible:ansible" | chpasswd && adduser ansible sudo

WORKDIR /home/ansible

RUN chown -R ansible:ansible /home/ansible

USER ansible

CMD ["/bin/bash"]