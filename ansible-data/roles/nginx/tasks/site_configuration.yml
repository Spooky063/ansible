---
- name: Set PHP-FPM service name
  set_fact:
    php_fpm_service_name: "php{{ item.php_version }}-fpm"

- name: Configure htpasswd
  block:
    - name: Create htpasswd file
      apt:
        pkg:
          - apache2-utils
          - python3-passlib
        state: present
        update_cache: yes

    - name: Configure htpasswd credentials
      community.general.htpasswd:
        path: /etc/nginx/.htpasswd
        name: "{{ htpasswd_user.username }}"
        password: "{{ htpasswd_user.password }}"
        crypt_scheme: bcrypt
      loop: "{{ item.htpasswd }}"
      loop_control:
        loop_var: htpasswd_user
      no_log: true
  when: item.htpasswd is defined and item.htpasswd | length > 0

- name: Ensure Nginx HTTP configuration files are present
  template:
    src: vhost.j2
    dest: "/etc/nginx/sites-available/{{ item.server_name }}"
    mode: '0644'
  when: not item.ssl

- name: Ensure Nginx HTTPS configuration files are present
  template:
    src: vhost-https.j2
    dest: "/etc/nginx/sites-available/{{ item.server_name }}"
    mode: '0644'
  when: item.ssl

- name: Configure SSL certificates
  include_tasks: ssl_certificates.yml
  when: item.ssl == "selfsigned"

- name: Configure Let's Encrypt certificates
  include_tasks: ssl_letsencrypt.yml
  when: item.ssl == "letsencrypt"

- name: Ensure Nginx sites are enabled
  file:
    src: "/etc/nginx/sites-available/{{ item.server_name }}"
    dest: "/etc/nginx/sites-enabled/{{ item.server_name }}"
    state: link
  notify:
    - php-fpm reload
    - nginx reload